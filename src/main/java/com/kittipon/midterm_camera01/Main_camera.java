/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.midterm_camera01;

import java.util.Scanner;

/**
 *
 * @author kitti
 */
public class Main_camera {

    public static void main(String[] args) {
        Camera camera = new Camera();
        Canon canon = new Canon();
        Sony sony = new Sony();
//        canon.ShowMemory();
        Scanner kb = new Scanner(System.in);
        char selectCamera = ' ';
        boolean turn = false;
        System.out.println("Type (Y) to turn NORMAL camera on.");
        System.out.println("Type (C) to turn CANON cameara on.");
        System.out.println("Type (S) to turn SONY cameara on.");
        while (turn == false) {
//            System.out.println("check");
            cameraStatus(turn);
            char status = Input(kb);
            selectCamera = status;
            turn = turnOn(status, turn);
        }
        switch (selectCamera) {
            case 'Y':
                while (true) {
                    camera.said();
                    showFunction();
                    String verb = Doing(kb);
//            System.out.println("check loop");
                    if (verb.length() == 1) {
                        String Do = verb;
                        if (turnOff(Do)) {
                            break;
                        }
                        camera.Press(Do, turn);
                    } else {
//                System.out.println("more than 1");
                        String Do = verb.substring(0, 1);
                        String X = verb.substring(1);
                        int number = Integer.parseInt(X);
                        camera.Press(Do, number, turn);
                    }

                }
                break;
            case 'C':
                while (true) {
                    canon.said();
                    showFunction();
                    String verb = Doing(kb);
//            System.out.println("check loop");
                    if (verb.length() == 1) {
                        String Do = verb;
                        if (turnOff(Do)) {
                            break;
                        }
                        canon.Press(Do, turn);
                    } else {
//                System.out.println("more than 1");
                        String Do = verb.substring(0, 1);
                        String X = verb.substring(1);
                        int number = Integer.parseInt(X);
                        canon.Press(Do, number, turn);
                    }

                }
                break;
            case 'S':
                while (true) {
                    sony.said();
                    showFunction();
                    String verb = Doing(kb);
//            System.out.println("check loop");
                    if (verb.length() == 1) {
                        String Do = verb;
                        if (turnOff(Do)) {
                            break;
                        }
                        sony.Press(Do, turn);
                    } else {
//                System.out.println("more than 1");
                        String Do = verb.substring(0, 1);
                        String X = verb.substring(1);
                        int number = Integer.parseInt(X);
                        sony.Press(Do, number, turn);
                    }

                }
                break;

        }

    }

    private static void showFunction() {
        System.out.println("Type (S) to take one picture burst shot");
        System.out.println("Type (B with number) to burst shot *max 10*");
        System.out.println("Type (D) to Delete a picture");
        System.out.println("Type (D with number) to increase memory you want");
        System.out.println("Type (F) to turn off this camera");
    }

    private static void cameraStatus(boolean turn) {
        System.out.print("Camera status : ");
        if (turn == false) {
            System.out.println("OFF");
        } else {
            System.out.println("ON");
        }
    }

    private static boolean turnOff(String Do) {
        if (Do.equals("F")) {
            System.out.println("Camera turn off");
            return true;
        }
        return false;
    }

    private static boolean turnOn(char status, boolean turn) {
        if (status == 'Y' || status == 'C' || status == 'S') {
            System.out.println("Camera turn on");
            turn = true;
        } else {
            System.out.println("Error!!!");
        }
        return turn;
    }

    private static char Input(Scanner kb) {
        String str = kb.next();
        return str.charAt(0);
    }

    private static String Doing(Scanner kb) {
//        System.out.println("check Doing");
        String str = kb.next();
        return str;
    }
}
